﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number=0;
            NumberToWord myObj = new NumberToWord();
            Console.WriteLine("Dear user, please give me an Arabic number:");
            try { 
                number=Int32.Parse(Console.ReadLine());
            }
            catch(FormatException fe)
            {
                Console.WriteLine("Input must be an interger \nEND!");
                Console.ReadLine();
                return;
            }
            Console.WriteLine(string.Format("{0}\t-->\t{1}", number, myObj.convertNumberToWord(number)));
            Console.ReadLine();
        }
    }
}
