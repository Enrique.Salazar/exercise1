﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class NumberToWord
    {
        Dictionary<int, string> elementaryNumber;
        private enum bigNumbers {million=1000000,thousand=1000,hundred=100};
        /// <summary>
        /// conver Arabic number to phrase
        /// </summary>
        /// <param name="number">number sent by main program</param>
        /// <returns>number's name</returns>
        public string convertNumberToWord(int number)
        {
            StringBuilder word = new StringBuilder();
            elementaryNumber = new Dictionary<int, string>(){
                 {0,"zero"}
                ,{1,"one"},{2,"two"},{3,"three"},{4,"four" },{5,"five" },{6,"six"},{7,"seven"},{8,"eight"},{9,"nine"}
                ,{10,"ten"},{11,"eleven"},{12,"twelve"},{13,"thirteen"},{14,"fourteen"},{15,"fifteen"}
                ,{16,"sixteen"},{17,"seventeen"},{18,"eighteen"},{19,"nineteen"}
                ,{20,"twenty"},{30,"thirty"},{40,"forty"},{50,"fifty"},{60,"sixty"},{70,"seventy"},{80,"eighty"},{90,"ninety"}
            };
            if (number == 0)
                return "zero";
            if (number < 0)
                return string.Format("minus {0}", convertNumberToWord(Math.Abs(number)));
            if ((number / (int)bigNumbers.million) > 0)
            {
                word.Append(string.Format("{0} million ", convertNumberToWord(number / (int)bigNumbers.million)));
                number %= (int)bigNumbers.million;
            }
            if ((number / (int)bigNumbers.thousand) > 0)
            {
                //this part is for special requirements that they want to call numbers like 1800 as eighteen hundred, this part work for a number between 1001 & 1999(if you need other range, change it) 
                //if you want to use the other way like 1800 as one thousand and eight hundred, only leave code that is inside else( without "else" and "{"  "}" of course)
                if (number > 1000 && number < 2000)
                {
                    word.Append(string.Format("{0} hundred ", convertNumberToWord(number / (int)bigNumbers.hundred)));
                    number %= (int)bigNumbers.thousand;
                    number %= (int)bigNumbers.hundred;
                }
                else
                {
                    word.Append(string.Format("{0} thousand ", convertNumberToWord(number / (int)bigNumbers.thousand)));
                    number %= (int)bigNumbers.thousand;
                }
            }
            if ((number / (int)bigNumbers.hundred) > 0)
            {
                word.Append(string.Format("{0} hundred ", convertNumberToWord(number / (int)bigNumbers.hundred)));
                number %= (int)bigNumbers.hundred;
            }
            word.Append(getNumberBetween1And99(number,word.ToString()));
            return word.ToString();
        }
        /// <summary>
        /// return the word value between 1 and 99
        /// </summary>
        /// <param name="number"></param>
        /// <param name="word">the phrase before because we want to know whether I need to put "and" or nothing</param>
        /// <returns>number's name between 1 and 99 </returns>
        private string getNumberBetween1And99(int number,string word)
        {
            StringBuilder NumberName = new StringBuilder();
            if (number > 0)
            {
                if (word != "")
                    NumberName.Append("and ");
                if (number < 20)
                    NumberName.Append(elementaryNumber[number]);
                else if(number<=99)
                {
                    NumberName.Append(elementaryNumber[(number / 10) * 10]);
                    if (number % 10 > 0)
                        NumberName.Append(string.Format("-{0}", elementaryNumber[number % 10]));
                }
            }
            return NumberName.ToString();
        }
        
    }
}
